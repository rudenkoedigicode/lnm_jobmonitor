﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace JobsMonitor
{
    public partial class ExitForm : Form
    {
        public ExitForm()
        {
            InitializeComponent();
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void buttonExit_Click(object sender, EventArgs e)
        {
            if (StCl.PasswordForExit == textBoxPassword.Text)
            {
                DialogResult = DialogResult.OK;
                this.Close();
            }
            else
            {
                MessageBox.Show("Password incorrect. Try again.", "Password...");
                textBoxPassword.Focus();
            }
        }

        private void ExitForm_Shown(object sender, EventArgs e)
        {
            textBoxPassword.Focus();
        }
    }
}
