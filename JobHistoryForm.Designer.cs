﻿namespace JobsMonitor
{
    partial class JobHistoryForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(JobHistoryForm));
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.dgvJobHistory = new System.Windows.Forms.DataGridView();
            this.textBoxMessage = new System.Windows.Forms.TextBox();
            this.col_instance_id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_DateTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_Step_Name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_Message = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_Status = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.contextMenuForGrid = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.refreshToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvJobHistory)).BeginInit();
            this.contextMenuForGrid.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.dgvJobHistory);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.textBoxMessage);
            this.splitContainer1.Size = new System.Drawing.Size(846, 404);
            this.splitContainer1.SplitterDistance = 288;
            this.splitContainer1.TabIndex = 0;
            // 
            // dgvJobHistory
            // 
            this.dgvJobHistory.AllowUserToAddRows = false;
            this.dgvJobHistory.AllowUserToDeleteRows = false;
            this.dgvJobHistory.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvJobHistory.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.col_instance_id,
            this.col_DateTime,
            this.col_Step_Name,
            this.col_Message,
            this.col_Status});
            this.dgvJobHistory.ContextMenuStrip = this.contextMenuForGrid;
            this.dgvJobHistory.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvJobHistory.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvJobHistory.Location = new System.Drawing.Point(0, 0);
            this.dgvJobHistory.MultiSelect = false;
            this.dgvJobHistory.Name = "dgvJobHistory";
            this.dgvJobHistory.ReadOnly = true;
            this.dgvJobHistory.RowTemplate.Height = 24;
            this.dgvJobHistory.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvJobHistory.Size = new System.Drawing.Size(846, 288);
            this.dgvJobHistory.TabIndex = 0;
            this.dgvJobHistory.SelectionChanged += new System.EventHandler(this.dgvJobHistory_SelectionChanged);
            // 
            // textBoxMessage
            // 
            this.textBoxMessage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxMessage.Location = new System.Drawing.Point(0, 0);
            this.textBoxMessage.Multiline = true;
            this.textBoxMessage.Name = "textBoxMessage";
            this.textBoxMessage.ReadOnly = true;
            this.textBoxMessage.Size = new System.Drawing.Size(846, 112);
            this.textBoxMessage.TabIndex = 0;
            // 
            // col_instance_id
            // 
            this.col_instance_id.HeaderText = "instance_id";
            this.col_instance_id.Name = "col_instance_id";
            this.col_instance_id.ReadOnly = true;
            this.col_instance_id.Visible = false;
            // 
            // col_DateTime
            // 
            this.col_DateTime.HeaderText = "Date";
            this.col_DateTime.Name = "col_DateTime";
            this.col_DateTime.ReadOnly = true;
            this.col_DateTime.Width = 120;
            // 
            // col_Step_Name
            // 
            this.col_Step_Name.HeaderText = "Step Name";
            this.col_Step_Name.Name = "col_Step_Name";
            this.col_Step_Name.ReadOnly = true;
            this.col_Step_Name.Width = 150;
            // 
            // col_Message
            // 
            this.col_Message.HeaderText = "Message";
            this.col_Message.Name = "col_Message";
            this.col_Message.ReadOnly = true;
            this.col_Message.Width = 200;
            // 
            // col_Status
            // 
            this.col_Status.HeaderText = "Status";
            this.col_Status.Name = "col_Status";
            this.col_Status.ReadOnly = true;
            // 
            // contextMenuForGrid
            // 
            this.contextMenuForGrid.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextMenuForGrid.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.refreshToolStripMenuItem});
            this.contextMenuForGrid.Name = "contextMenuForGrid";
            this.contextMenuForGrid.Size = new System.Drawing.Size(152, 28);
            // 
            // refreshToolStripMenuItem
            // 
            this.refreshToolStripMenuItem.Name = "refreshToolStripMenuItem";
            this.refreshToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F5;
            this.refreshToolStripMenuItem.Size = new System.Drawing.Size(175, 24);
            this.refreshToolStripMenuItem.Text = "Refresh";
            this.refreshToolStripMenuItem.Click += new System.EventHandler(this.refreshToolStripMenuItem_Click);
            // 
            // JobHistoryForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(846, 404);
            this.Controls.Add(this.splitContainer1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "JobHistoryForm";
            this.Text = "Job History";
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvJobHistory)).EndInit();
            this.contextMenuForGrid.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.DataGridView dgvJobHistory;
        private System.Windows.Forms.TextBox textBoxMessage;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_instance_id;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_DateTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_Step_Name;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_Message;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_Status;
        private System.Windows.Forms.ContextMenuStrip contextMenuForGrid;
        private System.Windows.Forms.ToolStripMenuItem refreshToolStripMenuItem;
    }
}