﻿using System;
using System.Windows.Forms;
using StandartClassLibrary;
using StandartClassLibrary.ConnectDB;
using StandartClassLibrary.Logs;

namespace JobsMonitor
{
    public static class StCl
    {
        public static Logs_Class glbLogs { get; set; }
        public static Ini_Class glbINI { get; set; }
        public static SQL_Class glbSQL { get; set; }
        public static MailSender glbMailSender { get; set; }
        private static int _TimerInterval = 0;
        public static int TimerInterval {
            get
            {
                if (_TimerInterval == 0)
                    _TimerInterval = glbINI.ReadSettingsForm("Main", "TimerInterval", 60);
                return _TimerInterval;
            }
            set
            {
                if (_TimerInterval != value)
                {
                    _TimerInterval = value;
                    glbINI.WriteSettingsForm("Main", "TimerInterval", _TimerInterval);
                }
            }
        }
        private static string _PasswordForExit = null;
        public static string PasswordForExit
        {
            get
            {
                if (string.IsNullOrEmpty(_PasswordForExit))
                    _PasswordForExit = glbINI.ReadSettingsForm("Main", "PasswordForExit", "1");
                return _PasswordForExit;
            }
            set
            {
                if (_PasswordForExit != value)
                {
                    _PasswordForExit = value;
                    glbINI.WriteSettingsForm("Main", "PasswordForExit", _PasswordForExit);
                }
            }
        }

        public static bool IsNullOrEmpty(this string Source)
        {
            return string.IsNullOrEmpty(Source);
        }

        public static string GetFormat(int MaxValue)
        {
            string returnValue = "";
            if (MaxValue > 9)
                returnValue += "0";
            if (MaxValue > 99)
                returnValue += "0";
            if (MaxValue > 999)
                returnValue += "0";
            if (MaxValue > 9999)
                returnValue += "0";
            returnValue += "#";
            return returnValue;
        }
    }
}
