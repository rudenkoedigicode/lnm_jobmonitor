﻿using System;
using System.Data;
using System.Windows.Forms;

namespace JobsMonitor
{
    public partial class JobHistoryForm : Form
    {
        private enum eOutcomeStatus
        {
            Failed = 0,
            Succeeded,
            Retry,
            Canceled,
            In_progress,
            Unknown
        }

        public string Job_id;

        public JobHistoryForm(string job_id, string Job_Name)
        {
            InitializeComponent();
            Job_id  = job_id;
            this.Text = Job_Name;
            RefreshGrid();
        }

        private void dgvJobHistory_SelectionChanged(object sender, EventArgs e)
        {
            if (dgvJobHistory.SelectedRows.Count > 0 && dgvJobHistory.SelectedRows[0].Cells["col_message"].Value != null)
                textBoxMessage.Text = dgvJobHistory.SelectedRows[0].Cells["col_message"].Value.ToString();
        }

        private void refreshToolStripMenuItem_Click(object sender, EventArgs e)
        {
            RefreshGrid();
        }

        private void RefreshGrid()
        {
            string strSQL = $"SELECT * FROM [msdb].[dbo].[sysjobhistory] WHERE job_id = '{Job_id}' ORDER By instance_id DESC";
            DataTable _dt = StCl.glbSQL.GetDataTable(strSQL);
            dgvJobHistory.Rows.Clear();
            dgvJobHistory.Rows.Add(_dt.Rows.Count);
            int _rowIndex = 0;
            foreach (DataRow _row in _dt.Rows)
            {
                DataGridViewRow _vrow = dgvJobHistory.Rows[_rowIndex];
                //col_DateTime
                _vrow.Cells["col_instance_id"].Value = _row["instance_id"];
                string _date = _row["run_date"].ToString();
                string _time = _row["run_time"].ToString().PadLeft(6, '0');
                string _datetime = $"{_date.Substring(4, 2)}-{_date.Substring(6, 2)}-{_date.Substring(0, 4)} {_time.Substring(0, 2)}:{_time.Substring(2, 2)}:{_time.Substring(4, 2)}";
                _vrow.Cells["col_DateTime"].Value = _datetime;
                _vrow.Cells["col_Step_Name"].Value = _row["step_name"];
                _vrow.Cells["col_message"].Value = _row["message"];
                _vrow.Cells["col_Status"].Value = ((eOutcomeStatus)_row["run_status"]).ToString();
                _rowIndex++;
            }
        }
    }
}
