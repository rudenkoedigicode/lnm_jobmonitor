﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Mail;
using StandartClassLibrary;

namespace JobsMonitor
{
    public class MailSender
    {
        private readonly SmtpClient smtpClient;
        #region section eMail (SMTP properties)
        const string sectionMail = "Mail";

        private bool? _MailEnable = null;
        public bool MailEnable
        {
            get
            {
                if (_MailEnable == null)
                    _MailEnable = StCl.glbINI.ReadSettingsForm(sectionMail, "Enable", false);
                return (bool)_MailEnable;
            }
            set
            {
                if (_MailEnable != value)
                {
                    _MailEnable = value;
                    StCl.glbINI.WriteSettingsForm(sectionMail, "Enable", _MailEnable == true);
                }
            }
        }

        private string _SMTPserver = null;
        public string SMTPserver
        {
            get
            {
                if (_SMTPserver.IsNullOrEmpty())
                    _SMTPserver = StCl.glbINI.ReadSettingsForm(sectionMail, "SMTPserver", "");
                return _SMTPserver;
            }
            set
            {
                if (_SMTPserver != value)
                {
                    _SMTPserver = value;
                    StCl.glbINI.WriteSettingsForm(sectionMail, "SMTPserver", _SMTPserver);
                }
            }
        }

        private int? _SMTPport = null;
        public int SMTPport
        {
            get
            {
                if (_SMTPport == null)
                    _SMTPport = StCl.glbINI.ReadSettingsForm(sectionMail, "SMTPport", 25);
                return (int)_SMTPport;
            }
            set
            {
                if (_SMTPport != value)
                {
                    _SMTPport = value;
                    StCl.glbINI.WriteSettingsForm(sectionMail, "SMTPport", (int)_SMTPport);
                }
            }
        }

        private string _SMTPlogin = null;
        public string SMTPlogin
        {
            get
            {
                if (_SMTPlogin.IsNullOrEmpty())
                    _SMTPlogin = StCl.glbINI.ReadSettingsForm(sectionMail, "SMTPlogin", "");
                return _SMTPlogin;
            }
            set
            {
                if (_SMTPlogin != value)
                {
                    _SMTPlogin = value;
                    StCl.glbINI.WriteSettingsForm(sectionMail, "SMTPlogin", _SMTPlogin);
                }
            }
        }

        private string _SMTPpass = null;
        public string SMTPpassword
        {
            get
            {
                if (_SMTPpass.IsNullOrEmpty())
                    _SMTPpass = StCl.glbINI.ReadSettingsForm(sectionMail, "SMTPpass", "", Ini_Class.eEncryptMethods.TripleDES);
                return _SMTPpass;
            }
            set
            {
                if (_SMTPpass != value)
                {
                    _SMTPpass = value;
                    StCl.glbINI.WriteSettingsForm(sectionMail, "SMTPpass", _SMTPpass, Ini_Class.eEncryptMethods.TripleDES);
                }
            }
        }

        private string _MailAddresser = null;
        public string MailAddresser
        {
            get
            {
                if (_MailAddresser.IsNullOrEmpty())
                    _MailAddresser = StCl.glbINI.ReadSettingsForm(sectionMail, "Addresser", "");
                return _MailAddresser;
            }
            set
            {
                if (_MailAddresser != value)
                {
                    _MailAddresser = value;
                    StCl.glbINI.WriteSettingsForm(sectionMail, "Addresser", _MailAddresser);
                }
            }
        }

        private string _MailRecipients = null;
        public string MailRecipients
        {
            get
            {
                if (_MailRecipients.IsNullOrEmpty())
                    _MailRecipients = StCl.glbINI.ReadSettingsForm(sectionMail, "Recipients", "");
                return _MailRecipients;
            }
            set
            {
                if (_MailRecipients != value)
                {
                    _MailRecipients = value;
                    StCl.glbINI.WriteSettingsForm(sectionMail, "Recipients", _MailRecipients);
                }
            }
        }

        // Когда был последний раз отправлен отчет об нерабочем SQL Agent
        private DateTime? _LastDateTimeSentReport = null;
        public DateTime LastDateTimeSentReport
        {
            get
            {
                if (_LastDateTimeSentReport == null)
                {
                    string _lastDateTimeSentReport = StCl.glbINI.ReadSettingsForm(sectionMail, "LastDateTimeSentReport", "2000-01-01 00:00:00");
                    _LastDateTimeSentReport = new DateTime(
                        Convert.ToInt16(_lastDateTimeSentReport.Substring(0, 4)),
                        Convert.ToInt16(_lastDateTimeSentReport.Substring(5, 2)),
                        Convert.ToInt16(_lastDateTimeSentReport.Substring(8, 2)),
                        Convert.ToInt16(_lastDateTimeSentReport.Substring(11, 2)),
                        Convert.ToInt16(_lastDateTimeSentReport.Substring(14, 2)),
                        Convert.ToInt16(_lastDateTimeSentReport.Substring(17, 2)));
                }
                return (DateTime)_LastDateTimeSentReport;
            }
            set
            {
                if (_LastDateTimeSentReport != value)
                {
                    _LastDateTimeSentReport = value;
                    StCl.glbINI.WriteSettingsForm(sectionMail, "LastDateTimeSentReport", ((DateTime)_LastDateTimeSentReport).ToString("yyyy-MM-dd HH:mm:ss"));
                }
            }
        }

        // Когда отправлять следующий отчет об нерабочем SQL Agent in minutes
        private int _RepeatSentReportAfter = -1;
        public int RepeatSentReportAfter
        {
            get
            {
                if (_RepeatSentReportAfter == -1)
                    _RepeatSentReportAfter = StCl.glbINI.ReadSettingsForm(sectionMail, "RepeatSentReportAfter", 60);
                return _RepeatSentReportAfter;
            }
            set
            {
                if (_RepeatSentReportAfter != value)
                {
                    _RepeatSentReportAfter = value;
                    StCl.glbINI.WriteSettingsForm(sectionMail, "RepeatSentReportAfter", _RepeatSentReportAfter);
                }
            }
        }
        #endregion

        public MailSender()
        {
            smtpClient = new SmtpClient("smtp.gmail.com", 587);
        }
        public MailSender(string host, int port)
        {
            if (!host.IsNullOrEmpty() || port > 0)
                smtpClient = new SmtpClient(host, port);
        }
        public MailSender(Ini_Class _ini)
        {
            if (!SMTPserver.IsNullOrEmpty() || SMTPport > 0)
                smtpClient = new SmtpClient(SMTPserver, SMTPport);
        }


        public void SendReport(string body, string subject, string mailAddresser = "")
        {
            if (smtpClient == null) return;
            if (string.IsNullOrEmpty(mailAddresser))
                mailAddresser = MailAddresser;

            var basicCredential = new NetworkCredential(SMTPlogin, SMTPpassword);
            smtpClient.Credentials = basicCredential;
            smtpClient.EnableSsl = true;
            smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;

            MailMessage message = new MailMessage();
            MailAddress mailAddress = new MailAddress(mailAddresser);
            string[] _recipients = MailRecipients.Split(new string[] { "," }, StringSplitOptions.None);

            message.To.Add(_recipients[0]);
            message.From = mailAddress;
            if (_recipients.Length > 1)
            {
                string recipients = string.Join(",", _recipients.Skip(1));
                message.CC.Add(recipients);
            }

            message.Subject = subject;

            message.Body = body;
            message.BodyEncoding = Encoding.UTF8;
            message.IsBodyHtml = false;

            smtpClient.Send(message);
        }
    }
}
