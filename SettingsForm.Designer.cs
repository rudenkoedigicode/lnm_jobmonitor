﻿namespace JobsMonitor
{
    partial class SettingsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SettingsForm));
            this.gbAuthentication = new System.Windows.Forms.GroupBox();
            this.chkWindowsAuthentication = new System.Windows.Forms.CheckBox();
            this.LabelUserID = new System.Windows.Forms.Label();
            this.LabelPassword = new System.Windows.Forms.Label();
            this.txtUserID = new System.Windows.Forms.TextBox();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnCheckConnection = new System.Windows.Forms.Button();
            this.txtDataBase = new System.Windows.Forms.TextBox();
            this.txtSQLServer = new System.Windows.Forms.TextBox();
            this.LabelDataBase = new System.Windows.Forms.Label();
            this.LabelSQLServer = new System.Windows.Forms.Label();
            this.btnCancel = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tpSQLserver = new System.Windows.Forms.TabPage();
            this.tpSMTP = new System.Windows.Forms.TabPage();
            this.buttonTestEmail = new System.Windows.Forms.Button();
            this.groupBoxEmail = new System.Windows.Forms.GroupBox();
            this.checkBoxSendByEmail = new System.Windows.Forms.CheckBox();
            this.textBoxSMTPport = new System.Windows.Forms.TextBox();
            this.textBoxSMTPaddress = new System.Windows.Forms.TextBox();
            this.textBoxSMTPpassword = new System.Windows.Forms.TextBox();
            this.labelSMTPport = new System.Windows.Forms.Label();
            this.labelSMTPaddress = new System.Windows.Forms.Label();
            this.textBoxAddresser = new System.Windows.Forms.TextBox();
            this.textBoxRecipients = new System.Windows.Forms.TextBox();
            this.textBoxSMTPlogin = new System.Windows.Forms.TextBox();
            this.labelSender = new System.Windows.Forms.Label();
            this.labelSMTPlogin = new System.Windows.Forms.Label();
            this.labelRecipients = new System.Windows.Forms.Label();
            this.labelSMTPpassword = new System.Windows.Forms.Label();
            this.tpOther = new System.Windows.Forms.TabPage();
            this.textBoxPasswordForExit = new System.Windows.Forms.TextBox();
            this.labelPasswordForExit = new System.Windows.Forms.Label();
            this.nudRepeatSendReportAfter = new System.Windows.Forms.NumericUpDown();
            this.labelRepeatSendReportAfter = new System.Windows.Forms.Label();
            this.checkBoxStartWithWindows = new System.Windows.Forms.CheckBox();
            this.labelMinutes = new System.Windows.Forms.Label();
            this.labelSeconds = new System.Windows.Forms.Label();
            this.nudTimerInterval = new System.Windows.Forms.NumericUpDown();
            this.labelTimeInterval = new System.Windows.Forms.Label();
            this.gbAuthentication.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tpSQLserver.SuspendLayout();
            this.tpSMTP.SuspendLayout();
            this.groupBoxEmail.SuspendLayout();
            this.tpOther.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudRepeatSendReportAfter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudTimerInterval)).BeginInit();
            this.SuspendLayout();
            // 
            // gbAuthentication
            // 
            this.gbAuthentication.Controls.Add(this.chkWindowsAuthentication);
            this.gbAuthentication.Controls.Add(this.LabelUserID);
            this.gbAuthentication.Controls.Add(this.LabelPassword);
            this.gbAuthentication.Controls.Add(this.txtUserID);
            this.gbAuthentication.Controls.Add(this.txtPassword);
            this.gbAuthentication.Location = new System.Drawing.Point(10, 75);
            this.gbAuthentication.Margin = new System.Windows.Forms.Padding(4);
            this.gbAuthentication.Name = "gbAuthentication";
            this.gbAuthentication.Padding = new System.Windows.Forms.Padding(4);
            this.gbAuthentication.Size = new System.Drawing.Size(433, 113);
            this.gbAuthentication.TabIndex = 28;
            this.gbAuthentication.TabStop = false;
            this.gbAuthentication.Text = "Authentication";
            // 
            // chkWindowsAuthentication
            // 
            this.chkWindowsAuthentication.AutoSize = true;
            this.chkWindowsAuthentication.Location = new System.Drawing.Point(121, 23);
            this.chkWindowsAuthentication.Margin = new System.Windows.Forms.Padding(4);
            this.chkWindowsAuthentication.Name = "chkWindowsAuthentication";
            this.chkWindowsAuthentication.Size = new System.Drawing.Size(179, 21);
            this.chkWindowsAuthentication.TabIndex = 0;
            this.chkWindowsAuthentication.Text = "&Windows authentication";
            this.chkWindowsAuthentication.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LabelUserID
            // 
            this.LabelUserID.Location = new System.Drawing.Point(15, 52);
            this.LabelUserID.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.LabelUserID.Name = "LabelUserID";
            this.LabelUserID.Size = new System.Drawing.Size(128, 20);
            this.LabelUserID.TabIndex = 1;
            this.LabelUserID.Text = "&User ID";
            // 
            // LabelPassword
            // 
            this.LabelPassword.Location = new System.Drawing.Point(15, 81);
            this.LabelPassword.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.LabelPassword.Name = "LabelPassword";
            this.LabelPassword.Size = new System.Drawing.Size(128, 20);
            this.LabelPassword.TabIndex = 3;
            this.LabelPassword.Text = "&Password";
            // 
            // txtUserID
            // 
            this.txtUserID.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtUserID.Location = new System.Drawing.Point(143, 52);
            this.txtUserID.Margin = new System.Windows.Forms.Padding(4);
            this.txtUserID.Name = "txtUserID";
            this.txtUserID.Size = new System.Drawing.Size(282, 22);
            this.txtUserID.TabIndex = 2;
            // 
            // txtPassword
            // 
            this.txtPassword.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtPassword.Location = new System.Drawing.Point(143, 81);
            this.txtPassword.Margin = new System.Windows.Forms.Padding(4);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.PasswordChar = '*';
            this.txtPassword.Size = new System.Drawing.Size(282, 22);
            this.txtPassword.TabIndex = 4;
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnSave.Location = new System.Drawing.Point(9, 341);
            this.btnSave.Margin = new System.Windows.Forms.Padding(4);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(85, 39);
            this.btnSave.TabIndex = 31;
            this.btnSave.Text = "&Save";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnCheckConnection
            // 
            this.btnCheckConnection.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnCheckConnection.Location = new System.Drawing.Point(7, 244);
            this.btnCheckConnection.Margin = new System.Windows.Forms.Padding(4);
            this.btnCheckConnection.Name = "btnCheckConnection";
            this.btnCheckConnection.Size = new System.Drawing.Size(146, 39);
            this.btnCheckConnection.TabIndex = 30;
            this.btnCheckConnection.Text = "&Test connection";
            this.btnCheckConnection.Click += new System.EventHandler(this.btnCheckConnection_Click);
            // 
            // txtDataBase
            // 
            this.txtDataBase.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDataBase.Location = new System.Drawing.Point(152, 44);
            this.txtDataBase.Margin = new System.Windows.Forms.Padding(4);
            this.txtDataBase.Name = "txtDataBase";
            this.txtDataBase.Size = new System.Drawing.Size(291, 22);
            this.txtDataBase.TabIndex = 27;
            // 
            // txtSQLServer
            // 
            this.txtSQLServer.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSQLServer.Location = new System.Drawing.Point(152, 15);
            this.txtSQLServer.Margin = new System.Windows.Forms.Padding(4);
            this.txtSQLServer.Name = "txtSQLServer";
            this.txtSQLServer.Size = new System.Drawing.Size(291, 22);
            this.txtSQLServer.TabIndex = 25;
            // 
            // LabelDataBase
            // 
            this.LabelDataBase.Location = new System.Drawing.Point(24, 44);
            this.LabelDataBase.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.LabelDataBase.Name = "LabelDataBase";
            this.LabelDataBase.Size = new System.Drawing.Size(128, 20);
            this.LabelDataBase.TabIndex = 26;
            this.LabelDataBase.Text = "&DataBase";
            // 
            // LabelSQLServer
            // 
            this.LabelSQLServer.Location = new System.Drawing.Point(24, 15);
            this.LabelSQLServer.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.LabelSQLServer.Name = "LabelSQLServer";
            this.LabelSQLServer.Size = new System.Drawing.Size(128, 20);
            this.LabelSQLServer.TabIndex = 24;
            this.LabelSQLServer.Text = "S&QL Server";
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(364, 341);
            this.btnCancel.Margin = new System.Windows.Forms.Padding(4);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(96, 39);
            this.btnCancel.TabIndex = 32;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tpSQLserver);
            this.tabControl1.Controls.Add(this.tpSMTP);
            this.tabControl1.Controls.Add(this.tpOther);
            this.tabControl1.Location = new System.Drawing.Point(9, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(458, 319);
            this.tabControl1.TabIndex = 33;
            // 
            // tpSQLserver
            // 
            this.tpSQLserver.Controls.Add(this.LabelSQLServer);
            this.tpSQLserver.Controls.Add(this.gbAuthentication);
            this.tpSQLserver.Controls.Add(this.btnCheckConnection);
            this.tpSQLserver.Controls.Add(this.LabelDataBase);
            this.tpSQLserver.Controls.Add(this.txtSQLServer);
            this.tpSQLserver.Controls.Add(this.txtDataBase);
            this.tpSQLserver.Location = new System.Drawing.Point(4, 25);
            this.tpSQLserver.Name = "tpSQLserver";
            this.tpSQLserver.Padding = new System.Windows.Forms.Padding(3);
            this.tpSQLserver.Size = new System.Drawing.Size(450, 290);
            this.tpSQLserver.TabIndex = 0;
            this.tpSQLserver.Text = "SQL Server";
            this.tpSQLserver.UseVisualStyleBackColor = true;
            // 
            // tpSMTP
            // 
            this.tpSMTP.Controls.Add(this.buttonTestEmail);
            this.tpSMTP.Controls.Add(this.groupBoxEmail);
            this.tpSMTP.Location = new System.Drawing.Point(4, 25);
            this.tpSMTP.Name = "tpSMTP";
            this.tpSMTP.Padding = new System.Windows.Forms.Padding(3);
            this.tpSMTP.Size = new System.Drawing.Size(450, 290);
            this.tpSMTP.TabIndex = 1;
            this.tpSMTP.Text = "SMTP server";
            this.tpSMTP.UseVisualStyleBackColor = true;
            // 
            // buttonTestEmail
            // 
            this.buttonTestEmail.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonTestEmail.Location = new System.Drawing.Point(19, 246);
            this.buttonTestEmail.Name = "buttonTestEmail";
            this.buttonTestEmail.Size = new System.Drawing.Size(112, 38);
            this.buttonTestEmail.TabIndex = 0;
            this.buttonTestEmail.Text = "Test eMail";
            this.buttonTestEmail.UseVisualStyleBackColor = true;
            this.buttonTestEmail.Click += new System.EventHandler(this.buttonTestEmail_Click);
            // 
            // groupBoxEmail
            // 
            this.groupBoxEmail.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBoxEmail.Controls.Add(this.checkBoxSendByEmail);
            this.groupBoxEmail.Controls.Add(this.textBoxSMTPport);
            this.groupBoxEmail.Controls.Add(this.textBoxSMTPaddress);
            this.groupBoxEmail.Controls.Add(this.textBoxSMTPpassword);
            this.groupBoxEmail.Controls.Add(this.labelSMTPport);
            this.groupBoxEmail.Controls.Add(this.labelSMTPaddress);
            this.groupBoxEmail.Controls.Add(this.textBoxAddresser);
            this.groupBoxEmail.Controls.Add(this.textBoxRecipients);
            this.groupBoxEmail.Controls.Add(this.textBoxSMTPlogin);
            this.groupBoxEmail.Controls.Add(this.labelSender);
            this.groupBoxEmail.Controls.Add(this.labelSMTPlogin);
            this.groupBoxEmail.Controls.Add(this.labelRecipients);
            this.groupBoxEmail.Controls.Add(this.labelSMTPpassword);
            this.groupBoxEmail.Location = new System.Drawing.Point(19, 6);
            this.groupBoxEmail.Name = "groupBoxEmail";
            this.groupBoxEmail.Size = new System.Drawing.Size(425, 221);
            this.groupBoxEmail.TabIndex = 3;
            this.groupBoxEmail.TabStop = false;
            this.groupBoxEmail.Text = "Email";
            // 
            // checkBoxSendByEmail
            // 
            this.checkBoxSendByEmail.AutoSize = true;
            this.checkBoxSendByEmail.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBoxSendByEmail.Location = new System.Drawing.Point(9, 21);
            this.checkBoxSendByEmail.Name = "checkBoxSendByEmail";
            this.checkBoxSendByEmail.Size = new System.Drawing.Size(161, 21);
            this.checkBoxSendByEmail.TabIndex = 0;
            this.checkBoxSendByEmail.Text = "Send report by eMail";
            this.checkBoxSendByEmail.UseVisualStyleBackColor = true;
            // 
            // textBoxSMTPport
            // 
            this.textBoxSMTPport.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxSMTPport.Location = new System.Drawing.Point(118, 76);
            this.textBoxSMTPport.Name = "textBoxSMTPport";
            this.textBoxSMTPport.Size = new System.Drawing.Size(301, 22);
            this.textBoxSMTPport.TabIndex = 4;
            // 
            // textBoxSMTPaddress
            // 
            this.textBoxSMTPaddress.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxSMTPaddress.Location = new System.Drawing.Point(118, 48);
            this.textBoxSMTPaddress.Name = "textBoxSMTPaddress";
            this.textBoxSMTPaddress.Size = new System.Drawing.Size(301, 22);
            this.textBoxSMTPaddress.TabIndex = 2;
            // 
            // textBoxSMTPpassword
            // 
            this.textBoxSMTPpassword.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxSMTPpassword.Location = new System.Drawing.Point(118, 133);
            this.textBoxSMTPpassword.Name = "textBoxSMTPpassword";
            this.textBoxSMTPpassword.PasswordChar = '*';
            this.textBoxSMTPpassword.Size = new System.Drawing.Size(301, 22);
            this.textBoxSMTPpassword.TabIndex = 8;
            // 
            // labelSMTPport
            // 
            this.labelSMTPport.AutoSize = true;
            this.labelSMTPport.Location = new System.Drawing.Point(6, 79);
            this.labelSMTPport.Name = "labelSMTPport";
            this.labelSMTPport.Size = new System.Drawing.Size(79, 17);
            this.labelSMTPport.TabIndex = 3;
            this.labelSMTPport.Text = "SMTP port:";
            // 
            // labelSMTPaddress
            // 
            this.labelSMTPaddress.AutoSize = true;
            this.labelSMTPaddress.Location = new System.Drawing.Point(6, 51);
            this.labelSMTPaddress.Name = "labelSMTPaddress";
            this.labelSMTPaddress.Size = new System.Drawing.Size(106, 17);
            this.labelSMTPaddress.TabIndex = 1;
            this.labelSMTPaddress.Text = "SMTP Address:";
            // 
            // textBoxAddresser
            // 
            this.textBoxAddresser.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxAddresser.Location = new System.Drawing.Point(118, 162);
            this.textBoxAddresser.Name = "textBoxAddresser";
            this.textBoxAddresser.Size = new System.Drawing.Size(301, 22);
            this.textBoxAddresser.TabIndex = 10;
            // 
            // textBoxRecipients
            // 
            this.textBoxRecipients.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxRecipients.Location = new System.Drawing.Point(118, 190);
            this.textBoxRecipients.Name = "textBoxRecipients";
            this.textBoxRecipients.Size = new System.Drawing.Size(301, 22);
            this.textBoxRecipients.TabIndex = 12;
            // 
            // textBoxSMTPlogin
            // 
            this.textBoxSMTPlogin.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxSMTPlogin.Location = new System.Drawing.Point(118, 104);
            this.textBoxSMTPlogin.Name = "textBoxSMTPlogin";
            this.textBoxSMTPlogin.Size = new System.Drawing.Size(301, 22);
            this.textBoxSMTPlogin.TabIndex = 6;
            // 
            // labelSender
            // 
            this.labelSender.AutoSize = true;
            this.labelSender.Location = new System.Drawing.Point(6, 162);
            this.labelSender.Name = "labelSender";
            this.labelSender.Size = new System.Drawing.Size(77, 17);
            this.labelSender.TabIndex = 9;
            this.labelSender.Text = "Addresser:";
            // 
            // labelSMTPlogin
            // 
            this.labelSMTPlogin.AutoSize = true;
            this.labelSMTPlogin.Location = new System.Drawing.Point(6, 107);
            this.labelSMTPlogin.Name = "labelSMTPlogin";
            this.labelSMTPlogin.Size = new System.Drawing.Size(84, 17);
            this.labelSMTPlogin.TabIndex = 5;
            this.labelSMTPlogin.Text = "SMTP login:";
            // 
            // labelRecipients
            // 
            this.labelRecipients.AutoSize = true;
            this.labelRecipients.Location = new System.Drawing.Point(6, 190);
            this.labelRecipients.Name = "labelRecipients";
            this.labelRecipients.Size = new System.Drawing.Size(78, 17);
            this.labelRecipients.TabIndex = 11;
            this.labelRecipients.Text = "Recipients:";
            // 
            // labelSMTPpassword
            // 
            this.labelSMTPpassword.AutoSize = true;
            this.labelSMTPpassword.Location = new System.Drawing.Point(6, 136);
            this.labelSMTPpassword.Name = "labelSMTPpassword";
            this.labelSMTPpassword.Size = new System.Drawing.Size(73, 17);
            this.labelSMTPpassword.TabIndex = 7;
            this.labelSMTPpassword.Text = "Password:";
            // 
            // tpOther
            // 
            this.tpOther.Controls.Add(this.textBoxPasswordForExit);
            this.tpOther.Controls.Add(this.labelPasswordForExit);
            this.tpOther.Controls.Add(this.nudRepeatSendReportAfter);
            this.tpOther.Controls.Add(this.labelRepeatSendReportAfter);
            this.tpOther.Controls.Add(this.checkBoxStartWithWindows);
            this.tpOther.Controls.Add(this.labelMinutes);
            this.tpOther.Controls.Add(this.labelSeconds);
            this.tpOther.Controls.Add(this.nudTimerInterval);
            this.tpOther.Controls.Add(this.labelTimeInterval);
            this.tpOther.Location = new System.Drawing.Point(4, 25);
            this.tpOther.Name = "tpOther";
            this.tpOther.Size = new System.Drawing.Size(450, 290);
            this.tpOther.TabIndex = 2;
            this.tpOther.Text = "Other";
            this.tpOther.UseVisualStyleBackColor = true;
            // 
            // textBoxPasswordForExit
            // 
            this.textBoxPasswordForExit.Location = new System.Drawing.Point(134, 98);
            this.textBoxPasswordForExit.Name = "textBoxPasswordForExit";
            this.textBoxPasswordForExit.PasswordChar = '*';
            this.textBoxPasswordForExit.Size = new System.Drawing.Size(117, 22);
            this.textBoxPasswordForExit.TabIndex = 7;
            // 
            // labelPasswordForExit
            // 
            this.labelPasswordForExit.AutoSize = true;
            this.labelPasswordForExit.Location = new System.Drawing.Point(13, 101);
            this.labelPasswordForExit.Name = "labelPasswordForExit";
            this.labelPasswordForExit.Size = new System.Drawing.Size(115, 17);
            this.labelPasswordForExit.TabIndex = 6;
            this.labelPasswordForExit.Text = "Password for exit";
            // 
            // nudRepeatSendReportAfter
            // 
            this.nudRepeatSendReportAfter.Increment = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.nudRepeatSendReportAfter.Location = new System.Drawing.Point(191, 70);
            this.nudRepeatSendReportAfter.Maximum = new decimal(new int[] {
            1440,
            0,
            0,
            0});
            this.nudRepeatSendReportAfter.Name = "nudRepeatSendReportAfter";
            this.nudRepeatSendReportAfter.Size = new System.Drawing.Size(60, 22);
            this.nudRepeatSendReportAfter.TabIndex = 5;
            // 
            // labelRepeatSendReportAfter
            // 
            this.labelRepeatSendReportAfter.AutoSize = true;
            this.labelRepeatSendReportAfter.Location = new System.Drawing.Point(13, 72);
            this.labelRepeatSendReportAfter.Name = "labelRepeatSendReportAfter";
            this.labelRepeatSendReportAfter.Size = new System.Drawing.Size(172, 17);
            this.labelRepeatSendReportAfter.TabIndex = 4;
            this.labelRepeatSendReportAfter.Text = "Repeat Send Report After";
            // 
            // checkBoxStartWithWindows
            // 
            this.checkBoxStartWithWindows.AutoSize = true;
            this.checkBoxStartWithWindows.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBoxStartWithWindows.Location = new System.Drawing.Point(13, 38);
            this.checkBoxStartWithWindows.Name = "checkBoxStartWithWindows";
            this.checkBoxStartWithWindows.Size = new System.Drawing.Size(144, 21);
            this.checkBoxStartWithWindows.TabIndex = 3;
            this.checkBoxStartWithWindows.Text = "Start with windows";
            this.checkBoxStartWithWindows.UseVisualStyleBackColor = true;
            this.checkBoxStartWithWindows.Visible = false;
            // 
            // labelMinutes
            // 
            this.labelMinutes.AutoSize = true;
            this.labelMinutes.Location = new System.Drawing.Point(257, 72);
            this.labelMinutes.Name = "labelMinutes";
            this.labelMinutes.Size = new System.Drawing.Size(57, 17);
            this.labelMinutes.TabIndex = 2;
            this.labelMinutes.Text = "minutes";
            // 
            // labelSeconds
            // 
            this.labelSeconds.AutoSize = true;
            this.labelSeconds.Location = new System.Drawing.Point(206, 12);
            this.labelSeconds.Name = "labelSeconds";
            this.labelSeconds.Size = new System.Drawing.Size(61, 17);
            this.labelSeconds.TabIndex = 2;
            this.labelSeconds.Text = "seconds";
            // 
            // nudTimerInterval
            // 
            this.nudTimerInterval.Increment = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.nudTimerInterval.Location = new System.Drawing.Point(130, 10);
            this.nudTimerInterval.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.nudTimerInterval.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.nudTimerInterval.Name = "nudTimerInterval";
            this.nudTimerInterval.Size = new System.Drawing.Size(70, 22);
            this.nudTimerInterval.TabIndex = 1;
            this.nudTimerInterval.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // labelTimeInterval
            // 
            this.labelTimeInterval.AutoSize = true;
            this.labelTimeInterval.Location = new System.Drawing.Point(13, 12);
            this.labelTimeInterval.Name = "labelTimeInterval";
            this.labelTimeInterval.Size = new System.Drawing.Size(94, 17);
            this.labelTimeInterval.TabIndex = 0;
            this.labelTimeInterval.Text = "Timer interval";
            // 
            // SettingsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(470, 393);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnCancel);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "SettingsForm";
            this.Text = "SettingsForm";
            this.Load += new System.EventHandler(this.SettingsForm_Load);
            this.gbAuthentication.ResumeLayout(false);
            this.gbAuthentication.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tpSQLserver.ResumeLayout(false);
            this.tpSQLserver.PerformLayout();
            this.tpSMTP.ResumeLayout(false);
            this.groupBoxEmail.ResumeLayout(false);
            this.groupBoxEmail.PerformLayout();
            this.tpOther.ResumeLayout(false);
            this.tpOther.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudRepeatSendReportAfter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudTimerInterval)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.GroupBox gbAuthentication;
        internal System.Windows.Forms.CheckBox chkWindowsAuthentication;
        internal System.Windows.Forms.Label LabelUserID;
        internal System.Windows.Forms.Label LabelPassword;
        internal System.Windows.Forms.TextBox txtUserID;
        internal System.Windows.Forms.TextBox txtPassword;
        internal System.Windows.Forms.Button btnSave;
        internal System.Windows.Forms.Button btnCheckConnection;
        internal System.Windows.Forms.TextBox txtDataBase;
        internal System.Windows.Forms.TextBox txtSQLServer;
        internal System.Windows.Forms.Label LabelDataBase;
        internal System.Windows.Forms.Label LabelSQLServer;
        internal System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tpSQLserver;
        private System.Windows.Forms.TabPage tpSMTP;
        private System.Windows.Forms.Button buttonTestEmail;
        private System.Windows.Forms.GroupBox groupBoxEmail;
        private System.Windows.Forms.CheckBox checkBoxSendByEmail;
        private System.Windows.Forms.TextBox textBoxSMTPport;
        private System.Windows.Forms.TextBox textBoxSMTPaddress;
        private System.Windows.Forms.TextBox textBoxSMTPpassword;
        private System.Windows.Forms.Label labelSMTPport;
        private System.Windows.Forms.Label labelSMTPaddress;
        private System.Windows.Forms.TextBox textBoxAddresser;
        private System.Windows.Forms.TextBox textBoxRecipients;
        private System.Windows.Forms.TextBox textBoxSMTPlogin;
        private System.Windows.Forms.Label labelSender;
        private System.Windows.Forms.Label labelSMTPlogin;
        private System.Windows.Forms.Label labelRecipients;
        private System.Windows.Forms.Label labelSMTPpassword;
        private System.Windows.Forms.TabPage tpOther;
        private System.Windows.Forms.Label labelSeconds;
        private System.Windows.Forms.NumericUpDown nudTimerInterval;
        private System.Windows.Forms.Label labelTimeInterval;
        private System.Windows.Forms.CheckBox checkBoxStartWithWindows;
        private System.Windows.Forms.NumericUpDown nudRepeatSendReportAfter;
        private System.Windows.Forms.Label labelRepeatSendReportAfter;
        private System.Windows.Forms.Label labelMinutes;
        private System.Windows.Forms.TextBox textBoxPasswordForExit;
        private System.Windows.Forms.Label labelPasswordForExit;
    }
}