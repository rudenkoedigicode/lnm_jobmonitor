﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Windows.Forms;
using StandartClassLibrary;
using StandartClassLibrary.Logs;
using StandartClassLibrary.ConnectDB;
using System.IO;

namespace JobsMonitor
{
    public partial class MonitorForm : Form
    {
        private bool exitApplication = false;
        //Current execution status:
        public enum eCurrentExecutionStatus
        {
            Executing = 1,
            Waiting_For_Thread,
            Between_Retries,
            Idle,
            Suspended,
            Obsolete,
            Performing_Completion_Actions
        }

        //Outcome of the job the last time it ran:
        public enum eLastRunOutcome
        {
            Failed = 0,
            Succeeded = 1,
            Canceled = 3,
            Unknown = 5
        }
        public MonitorForm()
        {
            try
            {
                InitializeComponent();
                #region dgvJobs
                dgvJobs.Columns.Add("col_job_id", "job_id");
                dgvJobs.Columns[0].Visible = false;
                dgvJobs.Columns.Add("col_name", "Name");
                dgvJobs.Columns.Add("col_enabled", "enabled");
                dgvJobs.Columns["col_enabled"].Width = 50;
                dgvJobs.Columns.Add("col_last_run", "last_run");
                dgvJobs.Columns["col_last_run"].Width = 120;
                dgvJobs.Columns.Add("col_last_run_outcome", "last_run_outcome");
                dgvJobs.Columns.Add("col_next_run", "next_run");
                dgvJobs.Columns["col_next_run"].Width = 120;
                dgvJobs.Columns.Add("col_status", "Status");
                dgvJobs.Columns["col_status"].Width = 50;
                dgvJobs.Columns.Add("col_operator", "Operator");
                #endregion
                AssemblyInfo_Class _assembly = new AssemblyInfo_Class(this);
                if (_assembly.InstanceExists(AssemblyInfo_Class.CheckMethods.byProccess))
                {
                    MessageBox.Show(_assembly.Product + " already running.");
                    this.Close();
                }
                _assembly.TypeOfVersion = AssemblyInfo_Class.TypesOfVersion.MajorAndMinor;
                this.Text = _assembly.TitleCaption;
                string jobMonitorPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "JobMonitor");
                StCl.glbINI = new Ini_Class();
                string _path = Path.Combine(jobMonitorPath, new FileInfo(StCl.glbINI.IniFileName).Name);
                StCl.glbINI.IniFileName = _path;

                StCl.glbLogs = new Logs_Class();
                StCl.glbLogs.PathLogs = jobMonitorPath;
                StCl.glbLogs.Open("Info", "SQL");
                StCl.glbLogs.WriteToLog("Start job monitor");
                StCl.glbSQL = new SQL_Class(StCl.glbLogs, 1, _assembly.Product, StCl.glbINI, null, Ini_Class.eEncryptMethods.TripleDES);
                StCl.glbMailSender = new MailSender(StCl.glbINI);
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            try
            {
                if (!StCl.glbSQL.ConnectToSQL())
                {
                    MessageBox.Show("Please configure SQL Server connection", "Connection...");
                }
                if (StCl.glbSQL.Connected)
                {
                    getSQLAgentStatus();
                    FillJobGrid();

                    timerCheck.Interval = StCl.TimerInterval * 1000;
                    if (timerCheck.Interval > 0)
                        timerCheck.Start();
                }
            }
            catch (Exception ex)
            {
                StCl.glbLogs.WriteToLog(ex);
            }
        }

        private void buttonSettings_Click(object sender, EventArgs e)
        {
            Boolean isRunning = timerCheck.Enabled;
            timerCheck.Stop();
            SettingsForm _form = new SettingsForm();
            if (_form.ShowDialog() == DialogResult.OK || isRunning)
            {
                getSQLAgentStatus();
                timerCheck.Interval = StCl.TimerInterval * 1000;
                if (timerCheck.Interval > 0)
                    timerCheck.Start();
            }
        }

        private void labelSQLAgentStatus_Click(object sender, EventArgs e)
        {
            getSQLAgentStatus();
        }
        private void refreshToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FillJobGrid();
        }

        private void getSQLAgentStatus()
        {
            try
            {

                string strReturn = "";
                if (StCl.glbSQL.ConnectToSQL())
                {
                    DataTable _dt = StCl.glbSQL.GetDataTable(@"SELECT dss.[servicename], dss.[status_desc], dss.status
FROM   sys.dm_server_services dss
WHERE  dss.[servicename] LIKE N'SQL Server Agent (%'; ");

                    if (_dt != null && _dt.Rows.Count > 0)
                    {
                        strReturn = _dt.Rows[0][0].ToString() + " - " + _dt.Rows[0][1].ToString();
                        if ((int)(_dt.Rows[0][2]) == 1)
                        {
                            DateTime _nextStart = StCl.glbMailSender.LastDateTimeSentReport.AddMinutes(StCl.glbMailSender.RepeatSentReportAfter);
                            if (_nextStart <= DateTime.Now)
                            {
                                if (StCl.glbMailSender.MailEnable)
                                {
                                    StCl.glbMailSender.SendReport(strReturn, strReturn);
                                }
                                StCl.glbMailSender.LastDateTimeSentReport = DateTime.Now;
                                notifyJobMonitor.ShowBalloonTip(7000, "Job Monitor", "SQL Agent stopped", ToolTipIcon.Warning);
                            }
                        }
                        else if (StCl.glbMailSender.LastDateTimeSentReport != new DateTime(2000, 1, 1))
                        {
                            StCl.glbMailSender.LastDateTimeSentReport = new DateTime(2000, 1, 1);
                        }
                    }
                }
                labelSQLAgentStatus.Text = strReturn;
            }
            catch (Exception ex)
            {
                StCl.glbLogs.WriteToLog(ex);
            }
        }

        private void FillJobGrid()
        {
            try
            {
                string[] _columnsJob = new string[] { "job_id", "name", "enabled", "last_run_",
                "last_run_outcome", "next_run_", "current_execution_status",
                "notify_email_operator" };
                StCl.glbSQL.ConnectToSQL();
                DataTable _dt = StCl.glbSQL.GetDataTable(@"exec msdb..sp_help_job");
                if (_dt != null)
                {
                    dgvJobs.Rows.Clear();
                    foreach (DataRow _item in _dt.Rows)
                    {
                        List<string> _newRowData = new List<string>();
                        foreach (string _columnName in _columnsJob)
                        {
                            if (_columnName == "current_execution_status")
                            {
                                eCurrentExecutionStatus _status = (eCurrentExecutionStatus)(_item[_columnName]);
                                _newRowData.Add(_status.ToString());
                            }
                            else if (_columnName == "last_run_outcome")
                            {
                                eLastRunOutcome _outcome = (eLastRunOutcome)(_item[_columnName]);
                                _newRowData.Add(_outcome.ToString());
                            }
                            else if (_columnName == "last_run_" || _columnName == "next_run_")
                            {
                                if ((int)_item[_columnName + "date"] == 0)
                                {
                                    _newRowData.Add("never");
                                }
                                else
                                {
                                    string _date = _item[_columnName + "date"].ToString();
                                    string _time = _item[_columnName + "time"].ToString().PadLeft(6, '0');
                                    string _dateTime = $"{_date.Substring(4, 2)}-{_date.Substring(6, 2)}-{_date.Substring(0, 4)}" +
                                        $" {_time.Substring(0, 2)}:{_time.Substring(2, 2)}:{_time.Substring(4, 2)}";
                                    _newRowData.Add(_dateTime);
                                }
                            }
                            else if (_columnName == "enabled")
                            {
                                if ((byte)_item[_columnName] == 1)
                                {
                                    _newRowData.Add("yes");
                                }
                                else
                                {
                                    _newRowData.Add("no");
                                }
                            }
                            else
                            {
                                _newRowData.Add(_item[_columnName].ToString());
                            }
                        }
                        dgvJobs.Rows.Add(_newRowData.ToArray());
                    }
                }
            }
            catch (Exception ex)
            {
                StCl.glbLogs.WriteToLog(ex);
            }
        }

        private void timerCheck_Tick(object sender, EventArgs e)
        {
            try
            {
                timerCheck.Stop();
                getSQLAgentStatus();
                FillJobGrid();
                timerCheck.Start();
            }
            catch (Exception ex)
            {
                StCl.glbLogs.WriteToLog(ex);
            }

        }
        #region Notification Icon
        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Normal;
            this.Show();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ExitForm _form = new ExitForm();
            if (_form.ShowDialog() == DialogResult.OK)
            {
                exitApplication = true;
                this.Close();
            }
        }

        private void MonitorForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!exitApplication)
            {
                e.Cancel = true;
                this.Hide();
            }
        }

        private void MonitorForm_SizeChanged(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Minimized)
            {
                this.Hide();
            }
        }
        #endregion
        #region Grid events
        private void dgvJobs_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            OpenHistory();
        }
        private void dgvJobs_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && !e.Shift && !e.Control && !e.Alt)
            {
                OpenHistory();
            }
        }
        private void openHistoryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenHistory();
        }
        private void OpenHistory()
        {
            string job_id = dgvJobs.SelectedRows[0].Cells[0].Value.ToString();
            string Job_name = dgvJobs.SelectedRows[0].Cells["col_name"].Value.ToString();
            timerCheck.Stop();
            JobHistoryForm _form = new JobHistoryForm(job_id, Job_name);
            _form.ShowDialog();
        }
        #endregion

    }
}
