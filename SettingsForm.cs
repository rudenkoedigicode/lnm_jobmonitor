﻿using System;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace JobsMonitor
{
    public partial class SettingsForm : Form
    {

        public SettingsForm()
        {
            InitializeComponent();
        }

        private void SettingsForm_Load(object sender, EventArgs e)
        {
            SqlConnectionStringBuilder _SqlConnectionBuilder = new SqlConnectionStringBuilder();
            try
            {
                #region SQL
                StCl.glbSQL.LoadSQLConnectionString(_SqlConnectionBuilder, 0);

                this.txtSQLServer.Text = _SqlConnectionBuilder.DataSource;
                this.txtDataBase.Text = _SqlConnectionBuilder.InitialCatalog;
                this.txtUserID.Text = _SqlConnectionBuilder.UserID;
                this.txtPassword.Text = _SqlConnectionBuilder.Password;

                if (this.txtUserID.Text.Length > 0)
                {
                    this.chkWindowsAuthentication.Checked = false;
                }
                else
                {
                    this.chkWindowsAuthentication.Checked = true;
                }
                #endregion
                #region SMTP
                checkBoxSendByEmail.Checked = StCl.glbMailSender.MailEnable;
                textBoxSMTPaddress.Text = StCl.glbMailSender.SMTPserver;
                textBoxSMTPport.Text = StCl.glbMailSender.SMTPport.ToString();
                textBoxSMTPlogin.Text = StCl.glbMailSender.SMTPlogin;
                textBoxSMTPpassword.Text = StCl.glbMailSender.SMTPpassword;
                textBoxAddresser.Text = StCl.glbMailSender.MailAddresser;
                textBoxRecipients.Text = StCl.glbMailSender.MailRecipients;
                #endregion
                #region Other
                nudTimerInterval.Value = StCl.TimerInterval;
                checkBoxStartWithWindows.Checked = StCl.glbINI.ReadSettingsForm("Main", "StartWithWindows", false);
                nudRepeatSendReportAfter.Value = StCl.glbMailSender.RepeatSentReportAfter;
                textBoxPasswordForExit.Text = StCl.PasswordForExit;
                #endregion
            }
            catch (Exception ex)
            {
                StCl.glbLogs.WriteToLog(ex);
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                #region SQL
                StCl.glbSQL.SQLServer = this.txtSQLServer.Text;
                StCl.glbSQL.SQLDatabase = this.txtDataBase.Text;

                StCl.glbSQL.SQLUserID = this.txtUserID.Text;
                StCl.glbSQL.SQLPassword = this.txtPassword.Text;
                #endregion
                #region SMTP
                StCl.glbMailSender.MailEnable = checkBoxSendByEmail.Checked;
                if (checkBoxSendByEmail.Checked && CheckTextBox(textBoxSMTPaddress) && CheckTextBox(textBoxSMTPport) &&
                    CheckTextBox(textBoxSMTPlogin) && CheckTextBox(textBoxSMTPpassword) && CheckTextBox(textBoxRecipients))
                {
                    return;
                }
                StCl.glbMailSender.SMTPserver = textBoxSMTPaddress.Text;
                int _SMTPport;
                if (int.TryParse(textBoxSMTPport.Text, out _SMTPport))
                {
                    StCl.glbMailSender.SMTPport = _SMTPport;
                }
                else
                {
                    StCl.glbMailSender.SMTPport = 0;
                    textBoxSMTPport.Text = "0";
                }
                StCl.glbMailSender.SMTPlogin = textBoxSMTPlogin.Text;
                StCl.glbMailSender.SMTPpassword = textBoxSMTPpassword.Text;
                StCl.glbMailSender.MailAddresser = textBoxAddresser.Text;
                StCl.glbMailSender.MailRecipients = textBoxRecipients.Text;
                #endregion
                #region Other
                StCl.TimerInterval = Convert.ToInt16(nudTimerInterval.Value);
                StCl.glbINI.WriteSettingsForm("Main", "StartWithWindows", checkBoxStartWithWindows.Checked);
                StCl.glbMailSender.RepeatSentReportAfter = Convert.ToInt16(nudRepeatSendReportAfter.Value);
                StCl.PasswordForExit = textBoxPasswordForExit.Text;
                #endregion
            }
            catch (Exception ex)
            {
                StCl.glbLogs.WriteToLog(ex);
            }
            this.DialogResult = DialogResult.OK;
            this.Close();

        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void btnCheckConnection_Click(object sender, EventArgs e)
        {
            bool blnConnection = false;
            string _SQLConnectionString = StCl.glbSQL.BuildConnectionString(this.txtSQLServer.Text, this.txtDataBase.Text, this.txtUserID.Text, this.txtPassword.Text);

            blnConnection = StCl.glbSQL.CheckSQLConnection(_SQLConnectionString);

            if (blnConnection)
            {
                MessageBox.Show("Test successfuly", "SQL connection", MessageBoxButtons.OK);
            }
            else
            {
                MessageBox.Show("Test failed", "SQL connection", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void buttonTestEmail_Click(object sender, EventArgs e)
        {
            try
            {
                MailSender _mailSender = new MailSender(textBoxSMTPaddress.Text, int.Parse(textBoxSMTPport.Text));
                _mailSender.SendReport("Test", "Test message", textBoxAddresser.Text);
                MessageBox.Show("Test successful");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Exception...");
            }

        }
        private Boolean CheckTextBox(TextBox Source)
        {
            if (Source.Text.IsNullOrEmpty())
            {
                MessageBox.Show("Can't be empty.", "Error...", MessageBoxButtons.OK);
                Source.Focus();
                return true;
            }
            return false;
        }

    }
}
